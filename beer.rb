class Beer

  def initialize(name, rating)
    @name = name
    @rating = rating
  end

  def display
    puts "Name : #{@name}, rating : #{@rating}"
  end
end